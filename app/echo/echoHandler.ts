import { NextFunction, Request, Response, Router } from 'express'
import * as createError from 'http-errors'
import { Logger } from 'winston'
function echoHandler(req: Request, res: Response, next: NextFunction) {
  return res.send({
    body: req.body || { empty: true },
    called: `${req.method}: /`,
    headers: req.headers,
    query: req.query
  })
}

function errorHandler(req: Request, res: Response, next: NextFunction) {
  var { status, message, ...otherProps } = req.body
  next(createError(status, message, otherProps))
}

export default function createRoutes(log: Logger) {
  var router: Router = Router()
  var handlerRoot = '/echo/'

  log.info(`Registering ${handlerRoot} routes`)

  router.post(handlerRoot, echoHandler)
  router.get(handlerRoot, echoHandler)
  router.put(handlerRoot, echoHandler)
  router.delete(handlerRoot, echoHandler)
  router.patch(handlerRoot, echoHandler)

  router.post(handlerRoot + 'error', errorHandler)

  return router
}
