import * as bodyParser from 'body-parser'
import { expect, use as chaiUse } from 'chai'
import * as express from 'express'
import * as sinon from 'sinon'
import * as sinonChai from 'sinon-chai'
import * as supertest from 'supertest'
import { createLogger, format, transports } from 'winston'
import { IConfig } from '../envConfig'
import { configureErrorHandler } from '../startup/errorHandler'
import createRoutes from './echoHandler'
chaiUse(sinonChai)

var log = createLogger({
  format: format.simple(),
  level: 'silly',
  transports: [new transports.Console({ silent: true })]
})
var configFake: IConfig

describe('Echo Handler', () => {
  var testSandbox: sinon.SinonSandbox
  beforeEach(() => {
    configFake = {
      logLevel: 'test',
      logRequestsEnabled: false,
      cleanHeaders: false,
      corsOrigins: ['*'],
      port: 1000
    }
    testSandbox = sinon.createSandbox()
  })
  afterEach(() => {
    testSandbox.restore()
  })
  describe('error echo', () => {
    var handler: express.Router
    var app: express.Express

    beforeEach(() => {
      handler = createRoutes(log)
      app = express()
      app.use(bodyParser.json())
      app.use(handler)
      app.use(configureErrorHandler(log, configFake).useHandler)
    })

    it('responds to a post', () => {
      return supertest(app)
        .post('/echo/error')
        .send({
          status: 599,
          message: 'a test message',
          custom: 'this should come in data property'
        })
        .expect(599)
        .then(res => {
          expect(res.body).to.deep.equal({
            status: 599,
            statusCode: 599,
            message: 'a test message',
            data: {
              custom: 'this should come in data property'
            }
          })
        })
    })
  })
  describe('echo action', () => {
    var handler: express.Router
    var app: express.Express

    beforeEach(() => {
      handler = createRoutes(log)
      app = express()
      app.use(handler)
    })
    it('responds to a post', () => {
      return supertest(app)
        .post('/echo')
        .expect(200)
        .then(res => {
          expect(res.body.called).to.eql('POST: /')
          expect(res.body.query).to.eql({})
        })
    })
    it('responds to a get', () => {
      return supertest(app)
        .get('/echo')
        .expect(200)
        .then(res => {
          expect(res.body.called).to.eql('GET: /')
          expect(res.body.query).to.eql({})
        })
    })

    it('responds to a put', () => {
      return supertest(app)
        .put('/echo')
        .expect(200)
        .then(res => {
          expect(res.body.called).to.eql('PUT: /')
          expect(res.body.query).to.eql({})
        })
    })
    it('responds to a delete', () => {
      return supertest(app)
        .delete('/echo')
        .expect(200)
        .then(res => {
          expect(res.body.called).to.eql('DELETE: /')
          expect(res.body.query).to.eql({})
        })
    })
  })
})
