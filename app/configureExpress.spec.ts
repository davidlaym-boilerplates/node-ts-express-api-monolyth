import { expect, use as chaiUse } from 'chai'
import * as express from 'express'
// tslint:disable-next-line:no-duplicate-imports
import { RequestHandler, Router } from 'express'
import 'mocha'
import * as sinon from 'sinon'
import * as sinonChai from 'sinon-chai'
import { createLogger, format, transports } from 'winston'
import { configureExpress } from './configureExpress'
import { IConfig } from './envConfig'
import { IConfigModule } from './startup/IConfigModule'

chaiUse(sinonChai)
var configFake: IConfig

var log = createLogger({
  format: format.simple(),
  level: 'silly',
  transports: [new transports.Console({ silent: true })]
})

describe('Express Configuration', () => {
  var testSandbox: sinon.SinonSandbox

  beforeEach(() => {
    configFake = {
      logLevel: 'test',
      logRequestsEnabled: false,
      cleanHeaders: false,
      corsOrigins: ['*'],
      port: 1000
    }
    testSandbox = sinon.createSandbox()
  })
  afterEach(() => {
    testSandbox.restore()
  })
  describe('with no middleware or roruter to configure', () => {
    it('registers error handler as last call', () => {
      var middlewares = []
      var router = null
      var app = express()
      var appUseSpy = testSandbox.spy(app, 'use')
      // var appOptionsSpy = testSandbox.spy(app, 'use');

      configureExpress(log, configFake, app, middlewares, router)
      // tslint:disable-next-line:no-unused-expression
      expect(appUseSpy).to.be.calledTwice
      expect(appUseSpy.lastCall.args[0])
        .to.be.a('function')
        .with.property('name')
        .equal('errorHandlerMiddleware')
    })

    it('registers not found handler as first call', () => {
      var middlewares = []
      var router = null
      var app = express()
      var appUseSpy = testSandbox.spy(app, 'use')
      // var appOptionsSpy = testSandbox.spy(app, 'use');

      configureExpress(log, configFake, app, middlewares, router)
      // tslint:disable-next-line:no-unused-expression
      expect(appUseSpy).to.be.calledTwice

      expect(appUseSpy.firstCall.args[0])
        .to.be.a('function')
        .with.property('name')
        .equal('notFoundMiddleware')
    })
  })

  describe('with no middleware but with routes', () => {
    it('registers error handler as last call', () => {
      var middlewares = []
      var router = Router()
      var app = express()
      var appUseSpy = testSandbox.spy(app, 'use')

      configureExpress(log, configFake, app, middlewares, router)
      // tslint:disable-next-line:no-unused-expression
      expect(appUseSpy).to.be.calledThrice
      expect(appUseSpy.lastCall.args[0])
        .to.be.a('function')
        .with.property('name')
        .equal('errorHandlerMiddleware')
    })

    it('registers not found handler as second call', () => {
      var middlewares = []
      var router = Router()
      var app = express()
      var appUseSpy = testSandbox.spy(app, 'use')

      configureExpress(log, configFake, app, middlewares, router)
      // tslint:disable-next-line:no-unused-expression
      expect(appUseSpy).to.be.calledThrice

      expect(appUseSpy.getCall(1).args[0])
        .to.be.a('function')
        .with.property('name')
        .equal('notFoundMiddleware')
    })
    it('adds router as first call', () => {
      var middlewares = []
      var router = Router()
      var app = express()
      var appUseSpy = testSandbox.spy(app, 'use')

      configureExpress(log, configFake, app, middlewares, router)
      // tslint:disable-next-line:no-unused-expression
      expect(appUseSpy).to.be.calledThrice

      expect(appUseSpy.firstCall).to.be.calledWithExactly(router)
    })
  })

  describe('with middleware', () => {
    var passThroughMdw: RequestHandler = function passTrhough(req, res, nx) {
      return nx()
    }
    it('registers error handler as last call', () => {
      var mdw1: IConfigModule = (mlog, config) => {
        return { useHandler: [passThroughMdw], routeHandlers: [] }
      }
      var middlewares = [mdw1]
      var router = Router()
      var app = express()
      var appUseSpy = testSandbox.spy(app, 'use')

      configureExpress(log, configFake, app, middlewares, router)
      // tslint:disable-next-line:no-unused-expression
      expect(appUseSpy).to.be.called
      expect(appUseSpy.lastCall.args[0])
        .to.be.a('function')
        .with.property('name')
        .equal('errorHandlerMiddleware')
    })
    describe('when middleware exposes only usehandlers', () => {
      it('registers global middleware as first call', () => {
        var mdw1: IConfigModule = (mlog, config) => {
          return { useHandler: [passThroughMdw], routeHandlers: [] }
        }
        var middlewares = [mdw1]
        var router = Router()
        var app = express()
        var appUseSpy = testSandbox.spy(app, 'use')

        configureExpress(log, configFake, app, middlewares, router)

        // tslint:disable-next-line:no-unused-expression
        expect(appUseSpy).to.be.called
        expect(appUseSpy.firstCall.args[0])
          .to.be.a('function')
          .with.property('name', 'passTrhough')
      })
    })
    describe('when middleware exposes only routeHandlers', () => {
      it('registers routesHanlers as first call (options verb)', () => {
        var mdw1: IConfigModule = (mlog, config) => {
          return {
            useHandler: [],
            routeHandlers: [
              { route: '*', handler: passThroughMdw, verb: 'options' }
            ]
          }
        }
        var middlewares = [mdw1]
        var router = Router()
        var app = express()
        var appOptionsSpy = testSandbox.spy(app, 'options')

        configureExpress(log, configFake, app, middlewares, router)

        // tslint:disable-next-line:no-unused-expression
        expect(appOptionsSpy).to.be.called
        expect(appOptionsSpy.firstCall.args[0]).equal('*')
        expect(appOptionsSpy.firstCall.args[1])
          .to.be.a('function')
          .with.property('name', 'passTrhough')
      })

      it('registers routesHanlers as first call (get verb)', () => {
        var mdw1: IConfigModule = (mlog, config) => {
          return {
            useHandler: [],
            routeHandlers: [
              { route: '*', handler: passThroughMdw, verb: 'get' }
            ]
          }
        }
        var middlewares = [mdw1]
        var router = Router()
        var app = express()
        var appOptionsSpy = testSandbox.spy(app, 'get')

        configureExpress(log, configFake, app, middlewares, router)

        // tslint:disable-next-line:no-unused-expression
        expect(appOptionsSpy).to.be.called
        expect(appOptionsSpy.firstCall.args[0]).equal('*')
        expect(appOptionsSpy.firstCall.args[1])
          .to.be.a('function')
          .with.property('name', 'passTrhough')
      })

      it('registers routesHanlers as first call (post verb)', () => {
        var mdw1: IConfigModule = (mlog, config) => {
          return {
            useHandler: [],
            routeHandlers: [
              { route: '*', handler: passThroughMdw, verb: 'post' }
            ]
          }
        }
        var middlewares = [mdw1]
        var router = Router()
        var app = express()
        var appOptionsSpy = testSandbox.spy(app, 'post')

        configureExpress(log, configFake, app, middlewares, router)

        // tslint:disable-next-line:no-unused-expression
        expect(appOptionsSpy).to.be.called
        expect(appOptionsSpy.firstCall.args[0]).equal('*')
        expect(appOptionsSpy.firstCall.args[1])
          .to.be.a('function')
          .with.property('name', 'passTrhough')
      })
      it('registers routesHanlers as first call (put verb)', () => {
        var mdw1: IConfigModule = (mlog, config) => {
          return {
            useHandler: [],
            routeHandlers: [
              { route: '*', handler: passThroughMdw, verb: 'put' }
            ]
          }
        }
        var middlewares = [mdw1]
        var router = Router()
        var app = express()
        var appOptionsSpy = testSandbox.spy(app, 'put')

        configureExpress(log, configFake, app, middlewares, router)

        // tslint:disable-next-line:no-unused-expression
        expect(appOptionsSpy).to.be.called
        expect(appOptionsSpy.firstCall.args[0]).equal('*')
        expect(appOptionsSpy.firstCall.args[1])
          .to.be.a('function')
          .with.property('name', 'passTrhough')
      })
      it('registers routesHanlers as first call (delete verb)', () => {
        var mdw1: IConfigModule = (mlog, config) => {
          return {
            useHandler: [],
            routeHandlers: [
              { route: '*', handler: passThroughMdw, verb: 'delete' }
            ]
          }
        }
        var middlewares = [mdw1]
        var router = Router()
        var app = express()
        var appOptionsSpy = testSandbox.spy(app, 'delete')

        configureExpress(log, configFake, app, middlewares, router)

        // tslint:disable-next-line:no-unused-expression
        expect(appOptionsSpy).to.be.called
        expect(appOptionsSpy.firstCall.args[0]).equal('*')
        expect(appOptionsSpy.firstCall.args[1])
          .to.be.a('function')
          .with.property('name', 'passTrhough')
      })
      it('registers routesHanlers as first call (patch verb)', () => {
        var mdw1: IConfigModule = (mlog, config) => {
          return {
            useHandler: [],
            routeHandlers: [
              { route: '*', handler: passThroughMdw, verb: 'patch' }
            ]
          }
        }
        var middlewares = [mdw1]
        var router = Router()
        var app = express()
        var appOptionsSpy = testSandbox.spy(app, 'patch')

        configureExpress(log, configFake, app, middlewares, router)

        // tslint:disable-next-line:no-unused-expression
        expect(appOptionsSpy).to.be.called
        expect(appOptionsSpy.firstCall.args[0]).equal('*')
        expect(appOptionsSpy.firstCall.args[1])
          .to.be.a('function')
          .with.property('name', 'passTrhough')
      })
    })

    describe('when middleware exposes both use and route handlers', () => {
      it('registers all use handlers first and then route handlers', () => {
        var mdw1: IConfigModule = (mlog, config) => {
          return {
            useHandler: [passThroughMdw, passThroughMdw],
            routeHandlers: [
              { route: '/test3', handler: passThroughMdw, verb: 'options' }
            ]
          }
        }
        var mdw2: IConfigModule = (mlog, config) => {
          return {
            useHandler: [passThroughMdw],
            routeHandlers: [
              { route: '/test2', handler: passThroughMdw, verb: 'get' },
              { route: '/test1', handler: passThroughMdw, verb: 'post' }
            ]
          }
        }
        var middlewares = [mdw1, mdw2]
        var router = Router()
        var app = express()

        configureExpress(log, configFake, app, middlewares, router)

        var middlewareDescriptors = getMiddlewareDescriptors(app)
        // tslint:disable-next-line:no-unused-expression
        expect(middlewareDescriptors).to.be.deep.equal([
          '[use] [*] passTrhough',
          '[use] [*] passTrhough',
          '[use] [*] passTrhough',
          'options /test3 passTrhough',
          'get /test2 passTrhough',
          'post /test1 passTrhough',
          '[use] [*] notFoundMiddleware',
          '[use] [*] errorHandlerMiddleware'
        ])
      })
    })
  })
})

function getMiddlewareDescriptors(app) {
  return app._router.stack
    .map(mdwmeta => {
      if (mdwmeta.name === 'bound dispatch') {
        var submeta = mdwmeta.route.stack[0]
        submeta.path = mdwmeta.route.path
        return { name: submeta.name, route: submeta }
      }
      return mdwmeta
    })
    .filter(mdwmeta => {
      return ['query', 'expressInit', 'router'].indexOf(mdwmeta.name) < 0
    })
    .map(mdwmeta => {
      var name = (mdwmeta.name || 'NO-NAME').trim()
      var route = mdwmeta.route || { methods: {}, path: '[*]' }
      var methods = (route.method ? [route.method] : Object.keys(route.methods))
        .map(m => m.trim() || 'USE')
        .join(', ')
      return `${methods || '[use]'} ${route.path.trim()} ${name}`
    })
}
