import { Router } from 'express'
import { Logger } from 'winston'
import echoHandler from './echo/echoHandler'

export default function createRoutes(log: Logger): Router {
  var router: Router = Router()
  var apiRoot = '/api'

  log.info('Registering routes')
  router.use(apiRoot, echoHandler(log))
  return router
}
