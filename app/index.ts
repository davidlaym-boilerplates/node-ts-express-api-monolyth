import * as dotenv from 'dotenv-safe'
import * as express from 'express'
import * as http from 'http'
import { createLogger, format, transports } from 'winston'
import { configureExpress } from './configureExpress'
import { getConfigFromEnv, IConfig } from './envConfig'
import { configureBodyParser } from './startup/bodyparser'
import { configureCompression } from './startup/compression'
import { configureCors } from './startup/cors'
import { configureHelmet } from './startup/helmet'
import { configureMorgan } from './startup/morgan'
// tslint:disable:no-console
console.log(`{"message":"Starting","level":"info"}`)
console.log(`{"message":"Loading environment variables","level":"info"}`)

dotenv.load()
var config = getConfigFromEnv()
console.log(
  `{"message":"Environment variables loaded","level":"info","config":${JSON.stringify(
    config || {}
  )}`
)

console.log(`{"message":"Using loglevel ${config.logLevel}","level":"info"}`)
// tslint:enable:no-console

var log = createLogger({
  format: format.json(),
  level: config.logLevel,
  transports: [new transports.Console()]
})

var app = express()

configureExpress(
  log,
  config,
  app,
  [
    configureMorgan,
    configureHelmet,
    configureCors,
    configureBodyParser,
    configureCompression
  ],
  express.Router()
)

var port = normalizePort(config.port || '3000')
var server = http.createServer(app)

server.listen(port)
server.on('error', onError)
server.on('listening', onListening)

process.on('unhandledRejection', onError)
process.on('uncaughtException', onError)
process.on('SIGINT', exitNormal('SIGINT'))
process.on('SIGTERM', exitNormal('SIGTERM'))

function exitNormal(signal: string) {
  return () => {
    log.info(`${signal} received, shutting down`)
    server.close(() => {
      log.info('Shutdown complete')
      process.exit(0)
    })
  }
}

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error
  }

  exitWithLogMessage(error)
}

function exitWithLogMessage(error: any) {
  var bindType = typeof port === 'string' ? 'Pipe' : ' Port'

  var message = getErrorMessage(error)
  exitAndLog(`${bindType} ${port} ${message}`)
}
var messages: {
  EACCES: 'requires elevated privileges'
  EADDRINUSE: 'is already in use'
}

function getErrorMessage(error) {
  var message = messages[error.code]
  if (!message) {
    throw error
  }
  return message
}

function exitAndLog(message) {
  log.error(message)
  process.exit(1)
}

function onListening() {
  var addr = server.address()
  var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port
  log.info('Listening on ' + bind)
}

function normalizePort(val) {
  var parsedPort = parseInt(val, 10)

  if (isNaN(parsedPort)) {
    return val
  }

  if (parsedPort >= 0) {
    return parsedPort
  }

  return false
}
