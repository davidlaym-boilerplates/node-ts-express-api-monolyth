import { ErrorRequestHandler, Express, RequestHandler, Router } from 'express'
import * as httpStatus from 'http-errors'
import { Logger } from 'winston'
import { IConfig } from './envConfig'
import createRoutes from './routes'
import { configureErrorHandler } from './startup/errorHandler'
import { IConfigModule } from './startup/IConfigModule'
export function configureExpress(
  log: Logger,
  config: IConfig,
  app: Express,
  globalMiddleware: IConfigModule[],
  rootRouter: Router
) {
  if (globalMiddleware && globalMiddleware.length > 0) {
    let useHandlers = []
    let routeHandlersStack = []
    for (let mdw of globalMiddleware) {
      let { useHandler, routeHandlers } = mdw(log, config)
      if (useHandler && useHandler.length > 0) {
        useHandlers.push(...useHandler)
      }
      if (routeHandlers && routeHandlers.length > 0) {
        routeHandlersStack.push(...routeHandlers)
      }
    }
    useHandlers.forEach(x => useIfDefined(app, x))
    routeHandlersStack.forEach(x => registerRoute(app, x))
  }
  if (!rootRouter) {
    log.warn('No root router received, will not register any route!')
  } else {
    rootRouter.use(createRoutes(log))
  }
  useIfDefined(app, rootRouter)

  app.use(notFoundMiddleware)

  useIfDefined(app, configureErrorHandler(log, config).useHandler[0])

  return app
}
function registerRoute(app, routeHandler) {
  switch (routeHandler.verb) {
    case 'options':
      app.options(routeHandler.route, routeHandler.handler)
      break
    case 'get':
      app.get(routeHandler.route, routeHandler.handler)
      break
    case 'post':
      app.post(routeHandler.route, routeHandler.handler)
      break
    case 'put':
      app.put(routeHandler.route, routeHandler.handler)
      break
    case 'delete':
      app.delete(routeHandler.route, routeHandler.handler)
      break
    case 'patch':
      app.patch(routeHandler.route, routeHandler.handler)
      break
    default:
      throw new Error(
        `could not register middleware for unknown verb ${routeHandler.verb}`
      )
  }
}
function notFoundMiddleware(req, resp, next) {
  next(new httpStatus.NotFound('Not Found'))
}

function useIfDefined(
  app,
  module:
    | RequestHandler
    | RequestHandler[]
    | ErrorRequestHandler
    | ErrorRequestHandler[]
) {
  if (module) {
    app.use(module)
  }
}
