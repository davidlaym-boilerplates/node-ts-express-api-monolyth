import * as cors from 'cors'
import { Logger } from 'winston'
import { IConfig } from '../envConfig'
import { IConfigModule } from './IConfigModule'

export var configureCors: IConfigModule = function configureCorsImpl(
  log: Logger,
  config: IConfig
) {
  var corscfg = config.corsOrigins
  log.debug('CORS Headers and routes are activated with provided config', {
    config: corscfg
  })
  var configuredCors = cors({ origin: corscfg })

  return {
    useHandler: [configuredCors],
    routeHandlers: [{ handler: configuredCors, route: '*', verb: 'options' }]
  }
}
