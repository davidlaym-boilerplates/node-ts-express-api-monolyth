import * as morgan from 'morgan'
import { Logger } from 'winston'
import { IConfig } from '../envConfig'
import { IConfigModule } from './IConfigModule'

export var configureMorgan: IConfigModule = function configureMorganImpl(
  log: Logger,
  config: IConfig
) {
  if (!config.logRequestsEnabled) {
    log.debug('Request logging not activated')
    return null
  }

  let myStream = new MyStream(log)

  return {
    useHandler: [
      morgan(
        '{"remote_addr": ":remote-addr", "remote_user": ":remote-user", ' +
          '"date": ":date[clf]", "method": ":method", "url": ":url", ' +
          '"http_version": ":http-version", "status": ":status", ' +
          '"result_length": ":res[content-length]", "referrer": ":referrer", ' +
          '"user_agent": ":user-agent", "response_time": ":response-time"}',
        { stream: myStream }
      )
    ],
    routeHandlers: []
  }
}

class MyStream {
  constructor(private log: Logger) {}
  write(text: string) {
    this.log.info(text)
  }
}
