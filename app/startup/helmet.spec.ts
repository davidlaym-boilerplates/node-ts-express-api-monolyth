import { expect, use as chaiUse } from 'chai'
import * as sinon from 'sinon'
import * as sinonchai from 'sinon-chai'
import { createLogger, format, transports } from 'winston'
import { IConfig } from '../envConfig'
import { configureHelmet } from './helmet'

chaiUse(sinonchai)
var configFake: IConfig

var log = createLogger({
  format: format.simple(),
  level: 'silly',
  transports: [new transports.Console({ silent: true })]
})
describe('Helmet Config', () => {
  var testSandbox: sinon.SinonSandbox

  beforeEach(() => {
    testSandbox = sinon.createSandbox()
    configFake = {
      logLevel: 'test',
      logRequestsEnabled: false,
      cleanHeaders: false,
      corsOrigins: ['*'],
      port: 1000
    }
  })
  afterEach(() => {
    testSandbox.restore()
  })
  it('returns null if config is not present', () => {
    var configured = configureHelmet(log, configFake)
    // tslint:disable-next-line:no-unused-expression
    expect(configured).to.be.null
  })
  it('returns null if config is present but disabled', () => {
    var configured = configureHelmet(log, configFake)
    // tslint:disable-next-line:no-unused-expression
    expect(configured).to.be.null
  })

  it('returns a configured module if config is present and enabled', () => {
    configFake.cleanHeaders = true
    var configured = configureHelmet(log, configFake)
    // tslint:disable-next-line:no-unused-expression
    expect(configured).not.to.be.null
    expect(configured)
      .has.property('useHandler')
      .that.is.a('array')
      .with.length(1)
    expect(configured)
      .has.property('routeHandlers')
      .that.is.a('array')
      .with.length(0)
  })
})
