import * as compression from 'compression'
import { Logger } from 'winston'
import { IConfig } from '../envConfig'
import { IConfigModule, IConfiguredModule } from './IConfigModule'

export var configureCompression: IConfigModule = function configureCompressionImpl(
  log: Logger,
  config: IConfig
): IConfiguredModule {
  return {
    useHandler: [compression()],
    routeHandlers: []
  }
}
