import * as bodyParser from 'body-parser'
import { IConfigModule } from './IConfigModule'

export var configureBodyParser: IConfigModule = function configureBodyParserImpl() {
  return {
    useHandler: [bodyParser.urlencoded({ extended: false }), bodyParser.json()],
    routeHandlers: []
  }
}
