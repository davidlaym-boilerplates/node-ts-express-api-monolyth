import { expect, use as chaiUse } from 'chai'
import 'mocha'
import * as sinon from 'sinon'
import * as sinonChai from 'sinon-chai'
import { createLogger, format, transports } from 'winston'
import { IConfig } from '../envConfig'
import { configureCors } from './cors'

chaiUse(sinonChai)

var log = createLogger({
  format: format.simple(),
  level: 'silly',
  transports: [new transports.Console({ silent: true })]
})

describe('Cors configuration', () => {
  var testSandbox: sinon.SinonSandbox
  var configFake: IConfig

  beforeEach(() => {
    configFake = {
      logLevel: 'test',
      logRequestsEnabled: false,
      cleanHeaders: false,
      corsOrigins: ['*'],
      port: 1000
    }
    testSandbox = sinon.createSandbox()
  })
  afterEach(() => {
    testSandbox.restore()
  })
  describe('with correct config', () => {
    it('returns configured module', () => {
      var configured = configureCors(log, configFake)
      // tslint:disable-next-line:no-unused-expression
      expect(configured)
        .has.property('useHandler')
        .that.is.a('array')
        .with.length(1)
      expect(configured)
        .has.property('routeHandlers')
        .that.is.a('array')
        .with.length(1)
    })
  })

  describe('with missing config', () => {
    it('returns null', () => {
      var configured = configureCors(log, configFake)
      // tslint:disable-next-line:no-unused-expression
      expect(configured).to.not.be.null
    })
  })
})
