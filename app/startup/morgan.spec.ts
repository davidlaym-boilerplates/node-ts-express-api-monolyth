import { expect, use as chaiUse } from 'chai'
import * as morgan from 'morgan'
import * as sinon from 'sinon'
import * as sinonchai from 'sinon-chai'
import { createLogger, format, transports } from 'winston'
import { IConfig } from '../envConfig'
import { configureMorgan } from './morgan'

chaiUse(sinonchai)

var log = createLogger({
  format: format.simple(),
  level: 'silly',
  transports: [new transports.Console({ silent: true })]
})
describe('Morgan Config', () => {
  var testSandbox: sinon.SinonSandbox
  var configFake: IConfig
  beforeEach(() => {
    configFake = {
      logLevel: 'test',
      logRequestsEnabled: false,
      cleanHeaders: false,
      corsOrigins: ['*'],
      port: 1000
    }
    testSandbox = sinon.createSandbox()
  })
  afterEach(() => {
    testSandbox.restore()
  })
  it('returns null if config is not present', () => {
    configFake.logRequestsEnabled = false

    var configured = configureMorgan(log, configFake)
    // tslint:disable-next-line:no-unused-expression
    expect(configured).to.be.null
  })

  it('returns configured module if enabled by config', () => {
    configFake.logRequestsEnabled = true
    var configured = configureMorgan(log, configFake)
    // tslint:disable-next-line:no-unused-expression
    expect(configured).not.to.be.null
    expect(configured)
      .has.property('useHandler')
      .that.is.a('array')
      .with.length(1)
    expect(configured)
      .has.property('routeHandlers')
      .that.is.a('array')
      .with.length(0)
  })
})
