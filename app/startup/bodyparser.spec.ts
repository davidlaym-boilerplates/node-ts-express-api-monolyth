import { expect, use as chaiUse } from 'chai'
import 'mocha'
import * as sinon from 'sinon'
import * as sinonChai from 'sinon-chai'
import { createLogger, format, transports } from 'winston'
import { IConfig } from '../envConfig'
import { configureBodyParser } from './bodyparser'

chaiUse(sinonChai)

var log = createLogger({
  format: format.simple(),
  level: 'silly',
  transports: [new transports.Console({ silent: true })]
})

describe('Bodyparser configuration', () => {
  var testSandbox: sinon.SinonSandbox
  var configFake: IConfig

  beforeEach(() => {
    configFake = {
      logLevel: 'test',
      logRequestsEnabled: false,
      cleanHeaders: false,
      corsOrigins: ['*'],
      port: 1000
    }
    testSandbox = sinon.createSandbox()
  })
  afterEach(() => {
    testSandbox.restore()
  })
  describe('when called', () => {
    it('return config for urlencoded and json', () => {
      var configured = configureBodyParser(null, null)

      // tslint:disable-next-line:no-unused-expression
      expect(configured).to.not.be.null
      expect(configured)
        .has.property('useHandler')
        .that.is.a('array')
        .with.length(2)

      expect(configured)
        .has.property('routeHandlers')
        .that.is.a('array')
        .with.length(0)

      expect(configured.useHandler[0]).to.be.a('function')
      expect(configured.useHandler[0].name).to.eql('urlencodedParser')

      expect(configured.useHandler[1]).to.be.a('function')
      expect(configured.useHandler[1].name).to.eql('jsonParser')
    })
  })
})
