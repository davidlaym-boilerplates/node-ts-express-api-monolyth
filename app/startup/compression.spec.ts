import { expect, use as chaiUse } from 'chai'
import 'mocha'
import * as sinon from 'sinon'
import * as sinonChai from 'sinon-chai'
import { createLogger, format, transports } from 'winston'
import { IConfig } from '../envConfig'
import { configureCompression } from './compression'

chaiUse(sinonChai)

var log = createLogger({
  format: format.simple(),
  level: 'silly',
  transports: [new transports.Console({ silent: true })]
})

describe('Compression configuration', () => {
  var testSandbox: sinon.SinonSandbox
  var configFake: IConfig

  beforeEach(() => {
    configFake = {
      logLevel: 'test',
      logRequestsEnabled: false,
      cleanHeaders: false,
      corsOrigins: ['*'],
      port: 1000
    }
    testSandbox = sinon.createSandbox()
  })
  afterEach(() => {
    testSandbox.restore()
  })
  describe('when called', () => {
    it('returns config for config module', () => {
      var configured = configureCompression(null, null)
      // tslint:disable-next-line:no-unused-expression
      expect(configured).to.not.be.null
      // tslint:disable-next-line:no-unused-expression
      expect(configured.routeHandlers).to.not.be.null
      expect(configured.routeHandlers.length).to.equal(0)

      // tslint:disable-next-line:no-unused-expression
      expect(configured.useHandler).to.not.be.null
      expect(configured.useHandler.length).to.equal(1)
      expect(configured.useHandler[0]).to.be.a('function')
    })
  })
})
