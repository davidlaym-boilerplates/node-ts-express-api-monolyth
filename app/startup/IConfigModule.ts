import { ErrorRequestHandler, RequestHandler } from 'express'
import { Logger } from 'winston'
import { IConfig } from '../envConfig'

export type IConfigModule = (
  log: Logger,
  config: IConfig
) => IConfiguredModule | null

export interface IConfiguredModule {
  useHandler: RequestHandler[] | ErrorRequestHandler[]
  routeHandlers: Array<{
    route: string
    handler: RequestHandler | ErrorRequestHandler
    verb: string
  }>
}
