import { expect, use as chaiUse } from 'chai'
import * as express from 'express'
import * as createError from 'http-errors'
import 'mocha'
import * as sinon from 'sinon'
import * as sinonChai from 'sinon-chai'
import { createLogger, format, transports } from 'winston'
import { IConfig } from '../envConfig'
import { configureErrorHandler } from './errorHandler'

chaiUse(sinonChai)
var configFake: IConfig

var log = createLogger({
  format: format.simple(),
  level: 'silly',
  transports: [new transports.Console({ silent: true })]
})

describe('errorHandler middleware configuration', () => {
  var testSandbox: sinon.SinonSandbox

  beforeEach(() => {
    configFake = {
      logLevel: 'test',
      logRequestsEnabled: false,
      cleanHeaders: false,
      corsOrigins: ['*'],
      port: 1000
    }
    testSandbox = sinon.createSandbox()
  })
  afterEach(() => {
    testSandbox.restore()
  })
  describe('allways', () => {
    it('returns configured module', () => {
      var configured = configureErrorHandler(log, configFake)
      // tslint:disable-next-line:no-unused-expression
      expect(configured)
        .has.property('useHandler')
        .that.is.a('array')
        .with.length(1)
      expect(configured)
        .has.property('routeHandlers')
        .that.is.a('array')
        .with.length(0)
    })
  })
})

describe('error handler implementation', () => {
  var testSandbox: sinon.SinonSandbox

  beforeEach(() => {
    configFake = {
      logLevel: 'test',
      logRequestsEnabled: false,
      cleanHeaders: false,
      corsOrigins: ['*'],
      port: 1000
    }
    testSandbox = sinon.createSandbox()
  })
  afterEach(() => {
    testSandbox.restore()
  })
  it('sets error code according to error status', () => {
    var configuredModule = configureErrorHandler(log, configFake)
    var handler = configuredModule.useHandler[0] as any
    var res = ({
      status: () => false,
      send: () => false,
      end: () => false
    } as any) as express.Response
    var req = null
    var nx = () => false

    var statusSpy = testSandbox.spy(res, 'status')
    var endSpy = testSandbox.spy(res, 'send')

    var err = {
      status: 999
    }
    handler(err, req, res, nx)
    // tslint:disable-next-line:no-unused-expression
    expect(statusSpy).to.be.calledOnceWithExactly(999)
    // tslint:disable-next-line:no-unused-expression
    expect(endSpy).to.be.calledOnce
  })

  it('if no error status is given it returns error 500', () => {
    var configuredModule = configureErrorHandler(log, configFake)
    var handler = configuredModule.useHandler[0] as any
    var res = ({
      status: () => false,
      end: () => false,
      send: () => false
    } as any) as express.Response
    var req = null
    var nx = () => false

    var statusSpy = testSandbox.spy(res, 'status')
    var endSpy = testSandbox.spy(res, 'send')

    var err = {}
    handler(err, req, res, nx)
    // tslint:disable-next-line:no-unused-expression
    expect(statusSpy).to.be.calledOnceWithExactly(500)
    // tslint:disable-next-line:no-unused-expression
    expect(endSpy).to.be.calledOnce
  })
  describe('understands correclty a HttpError', () => {
    it('send status code and message as json response', () => {
      var configuredModule = configureErrorHandler(log, configFake)
      var handler = configuredModule.useHandler[0] as any
      var res = ({
        status: () => false,
        send: () => false
      } as any) as express.Response
      var req = null
      var nx = () => false

      var statusSpy = testSandbox.spy(res, 'status')
      var sendSpy = testSandbox.spy(res, 'send')

      var err = createError(400, 'this is a test')

      handler(err, req, res, nx)
      // tslint:disable-next-line:no-unused-expression
      expect(statusSpy).to.be.calledOnceWithExactly(400)
      // tslint:disable-next-line:no-unused-expression
      expect(sendSpy).to.be.calledOnceWithExactly({
        statusCode: 400,
        status: 400,
        message: 'this is a test'
      })
    })
  })
  it('sends aditional data if is provided', () => {
    var configuredModule = configureErrorHandler(log, configFake)
    var handler = configuredModule.useHandler[0] as any
    var res = ({
      status: () => false,
      send: () => false
    } as any) as express.Response
    var req = null
    var nx = () => false

    var statusSpy = testSandbox.spy(res, 'status')
    var sendSpy = testSandbox.spy(res, 'send')

    var err = createError(400, 'this is a test', { someProperty: 'a value' })

    handler(err, req, res, nx)
    // tslint:disable-next-line:no-unused-expression
    expect(statusSpy).to.be.calledOnceWithExactly(400)
    // tslint:disable-next-line:no-unused-expression
    expect(sendSpy).to.be.calledOnceWithExactly({
      statusCode: 400,
      status: 400,
      message: 'this is a test',
      data: {
        someProperty: 'a value'
      }
    })
  })
})
