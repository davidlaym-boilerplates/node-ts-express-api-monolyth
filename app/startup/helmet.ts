import * as helmet from 'helmet'
import { Logger } from 'winston'
import { IConfig } from '../envConfig'
import { IConfigModule } from './IConfigModule'

export var configureHelmet: IConfigModule = function configureHelmetImpl(
  log: Logger,
  config: IConfig
) {
  if (config.cleanHeaders) {
    log.info('Enabling clean headers using helmet')
    return {
      useHandler: [helmet()],
      routeHandlers: []
    }
  } else {
    log.info('Clean headers are disabled by config.')
  }
  return null
}
