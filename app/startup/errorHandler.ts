import { ErrorRequestHandler, NextFunction, Request, Response } from 'express'
import { HttpError } from 'http-errors'
import { Logger } from '../../node_modules/winston'
import { IConfig } from '../envConfig'
import { IConfigModule } from './IConfigModule'

export var configureErrorHandler: IConfigModule = function configureErrorImpl(
  log: Logger,
  config: IConfig
) {
  return {
    useHandler: [
      function errorHandlerMiddleware(
        err: any,
        req: Request,
        res: Response,
        next: NextFunction
      ) {
        if (err instanceof HttpError) {
          var {
            message,
            status,
            statusCode,
            headers,
            expose,
            stack,
            name,
            ...props
          } = err

          res.status(status)
          var payload = { status, message, statusCode } as any
          if (props && Object.keys(props).length > 0) {
            payload.data = props
          }
          res.send(payload)
        } else {
          log.warn('Handled external error on middleware', err)

          res.status(err.status || 500)
          res.send({
            message: err.message,
            statusCode: err.status || 500
          })
        }
      } as ErrorRequestHandler
    ],
    routeHandlers: []
  }
}
