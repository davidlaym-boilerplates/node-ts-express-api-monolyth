import { expect, use as chaiUse } from 'chai'
import * as listEndpoints from 'express-list-endpoints'
import 'mocha'
import * as sinonChai from 'sinon-chai'
import { createLogger, format, transports } from 'winston'
import createRoutes from './routes'

chaiUse(sinonChai)

var log = createLogger({
  format: format.simple(),
  level: 'silly',
  transports: [new transports.Console({ silent: true })]
})

describe('main route files', () => {
  it('generates routes for echo hanlder', () => {
    var router = createRoutes(log)

    var endpoints = listEndpoints(router)

    expect(endpoints).to.deep.equal([
      { path: '/api/echo/', methods: ['POST'] },
      { path: '/api/echo/', methods: ['GET'] },
      { path: '/api/echo/', methods: ['PUT'] },
      { path: '/api/echo/', methods: ['DELETE'] },
      { path: '/api/echo/', methods: ['PATCH'] },
      { path: '/api/echo/error', methods: ['POST'] }
    ])
  })
})
