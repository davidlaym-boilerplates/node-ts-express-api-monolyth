# Typescript Express Boilerplate

## Requirements

- Node 8.11 or greater
- NPM 6.2 or greater

## Quick start (development lifecycle)

1.  run `npm install` to install dependencies
2.  run `npm run test` to execute tests and coverage report
3.  run `npm run tdd` to run a TDD session with file watching and min reporting
4.  run `npm run watch` to launch the application and wait for changes to restart it.
5.  run `npm run format` to apply formatting standard to all files under ./app.
6.  run `npm run tslint` to check code for lint problems.

Take note that this template use pre-commit hooks to ensure compliance with code
formatting standard.

## Code Formatting

We use Prettier to ensure consistent code-standard and to provide a good coding
experience. We recommend that you integrate Prettier to your code editor or IDE
for a complete no-friction usage.

We also use TSLint to ensure no-nonsense code and avoid common pitfalls using
typescript.

## Folder structure

This project assumes a very specific way of organizing code that is probably
not familiar to everybody. We use a `folder-per-resource` strategy that consists
in that any file specific to a given resource is co-located in the same folder
no matter what the file actually implements (controller, model, service, test,
whatever). File names in this strategy are much more important, because they
should clearly expose the responsibility of the code inside. Usually we follow
the nomenclature `<resource><Responsability>[.spec].ext` when the file relates
to a specific resource or just `<responsability>[.spec].ext` when it doesn't.

We don't use MVC nomenclature (Model/View/Controller) because in a API the view
just doesn't apply and in express we don't use controllers, we have handlers and
routers, so we stick to those terms. For models, we use them only if we a re
using a ORM that requires defining such models.

This code organization improves the discovery of related files by co-location and
simplifies the navigation both on the CLI by navigating the folders and in
GUI editors with file trees and/or navigational search mechanism. Also usually
keeps changes in a given commit co-located also in a particular folder.

### Root level files

We have some files for configuration of the development environment:

- `.editorconfig`: for editors supporting this file, it enforces up white space,
  line feed, encoding and other settings. It's very important and we suggest
  researching your code editor capabilities to enable support of this feature.
- `.env.example`: example configuration file with environment variables
- `.gitignore`: standard git configuration file for providing ignore patterns
- `.npmrc`: standard NPM configuration file for providing flags to the CLI utility
- `mocha.opts`: standard mocha file for providing command-line options to the CLI
  utility. Usually mocha expects the file to be in the `tests` folder but
  we don't have such folder since we co-locate tests and implementation.
- `nodemon.json`: configuration for the `nodemon` tool that watches for changes in
  files to restart a process.
- `package.json`: along with `package-lock.json` are standard NPM settings files for
  dependency and package management.
- `README.md`: this file.
- `tsconfig.json`: configuration for the typescript compiler
- `tslint.json`: configuration for the typescript linter

### `app` folder

In this folder is where all the working code should be located along with any
tests. There are some root files in this folder that are the entry point and
main configuration scripts for the application.

- `index.ts`: This is the application entry point. It sets up core dependencies and
  error handlers at the node level, starts the listening on http port and
  kicks off the express configuration.
- `routes.ts`: this file generates a express router with all the routes that
  are exposed. It may reach and require another sub-routers in along the nesting
  of the resources.
- `configureExpress.ts`: this file sets up express middleware and adds the router
  and sets up express-level error handlers

### `app/startup` folder

In this folder we locate all the startup scripts for the express configuration,
mainly the global middleware configuration.

### `app/echo` folder

Example simple echo route handler

### `buildFiles` folder

Files that are needed only for the build-time or post-build (execution). This
includes for example the production `Dockerfile`

### `docs` folder

This folder contains documentation for the API like the swagger file and possibly
other type of live or run-time documentation.

### Automatically generated folders and other non-tracked items

- `coverage` folder: Automatically generated folder that stores test coverage
  reports. It is not and should not be tracked by git.
- `node_modules` folder: NPM package dependencies.
- `.nyc_output` folder: temporary coverage information used by the nyc tool
- `dist` folder: Automatically generated folder that is created by the build
  process. It contains the Web API transpiled in JavaScript (source is on
  typescript)
- `maps` folder: Automatically generated folder that is created by the build
  process. It contains the source map files to associate TypeScript sources
  with the generated JavaScript files.

## Configuration

We use the NPM module `dotenv-safe` for loading environment variables
on development, but you should really provide environment variables on the host
for the API.

We provide `.env.example` file with every environment variable that is in use.
This file is also used to validate at runtime which variables should be present

### Logging

For logging we use two modules: `winston` and `morgan`. Winston is a very
powerful logging library. We can set the minimum level that's going to be
registered with the environment variable `LOG_LEVEL`.

Morgan is a express middleware that logs request to our endpoints in several
formats. For this project we integrated `morgan` with `winston`. `morgan` reports
every request to `winston` and `winston` logs in a structured way every request
variable. Requests are logged using the `info` severity.

You can modify this setup using the code in `/app/index.ts` for `winston`
and `/app/startup/morgan.ts` for `morgan`

### Security

There are many moving parts on security, this boilerplate sets up a couple of
low-hanging-fruit kind of practices to ensure your app is at least a little safe
by default. Particularly it sets up headers and some routes that helps to
secure your API from unwanted behavior.

We use mainly two packages for this: `helmet` and `cors`. Helmet using a default
configuration sets up several http headers for a safe default behavior. Is
configured only by the environment variable `CLEAN_HEADERS` that enables it
or disables it (enabled by default). Further customization has to be done on
the `/app/startup/helmet.ts` code.

With `cors` we set up an additional header for `Access-Control-Allow-Origin` and in
the configuration file we can modify allowed origins
with the environment variable `CORS_ORIGINS` that is a comma separated list
of valid origins to allow.

## Build Process

The build process is configures to use GitLab CI and the configuration for it
is on `.gitlab-ci.yml`

Is a 3 step build process compliant with 12 factor apps, and requires that
environment variables are present on the build environment. You can set
the variables on GitLab for a given environment on the project settings (go for the CI/CD config).

- In the first step `build` we generate a ready for production artifact, with
  transpiled to JavaScript and with only the necessary files
- In step two `unit tests` we execute unit tests against the production
  artifacts and generate coverage and tests reports
- In step three `verification` we execute API tests to ensure the API
  swagger documentation reflects the real endpoints.

If you need to test or modify the build process this are some of the built-in
scripts to look at:

1. `npm run build` runs a full `tslint`+`typescript` build and generates a
   `dist` folder ready to be copied over to production, including `node_modules`
2. `npm run test`executes the full suite of tests+coverage information
   generating a `JUnit` report (for TypeScript sources).
3. `npm start` starts a normal node process for this API using TypeScript Sources
4. `npm prod:start` **Run only on a JavaScript built artifact.** starts a
   normal node process for this API.
5. `npm run api-verify` **Run only on a JavaScript built artifact.** starts
   `dredd` which is a tool that reads the swagger docs (on `docs/api` folder)
   and also starts the node server with the API, then executes tests using
   the swagger documentation as template for requests.
6. `npm api-docs` uses `rdoc-cli`
   to generate an html version of the swagger YAML file and copies it to the
   docs folder

## Editors

This template is best used with visual studio code, but it's not dependent on
it. Every tool configured is tested from the command line and any plugin here
suggested is only thought to provide additional comfort or experience to the
developer and is not depended upon when building or deploying.

### Suggested Visual Studio Code plugins:

- [EditorConfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig):
  Enforces whitespace, line breaks and other conventions expressed on the
  `.editorconfig` file.
- [TSLint](https://marketplace.visualstudio.com/items?itemName=eg2.tslint)
  Runs `tslint` on open files highlighting errors while typing and provides
  fixes for common problems. It is configured in the `tslint.config` file
  to coincide with prettier conventions.
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
  Provides auto-formatting capabilities and is configured in `package.json` with
  some specific tweaks for this project
- [CodeMetrics](https://marketplace.visualstudio.com/items?itemName=kisstkondoros.vscode-codemetrics)
  Displays code cyclometric complexity stats for every function via CodeLens in
  the editor itself with very sane defaults
